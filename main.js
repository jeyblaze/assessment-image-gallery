var displayedImage = document.querySelector('.displayed-img');
var thumbBar = document.querySelector('.thumb-bar');

btn = document.querySelector('button');
var overlay = document.querySelector('.overlay');

for (var i = 0; i < 5; i++) {
  var newImage = document.createElement('img');
  newImage.setAttribute('src', "images/pic" + (i+1) + ".jpg");
  thumbBar.appendChild(newImage);
}

thumbBar.addEventListener('click', (e) => {
  if(e.target && e.target.nodeName == "IMG") {
    displayedImage.src = e.target.src;
}
});

btn.addEventListener('click', (e) => {
  if (e.target.className == 'dark') {
    btn.setAttribute('class', 'light');
    overlay.style.backgroundColor = 'rgba(0,0,0,0.5)';
}

 else {
    btn.setAttribute('class', 'dark');
    overlay.style.backgroundColor = 'rgba(0,0,0,0)';
}
}); 
